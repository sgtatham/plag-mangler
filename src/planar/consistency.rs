// plag-mangler - planar graph handling utility and layout optimiser
//
// consistency.rs - consistency check (NOT YET COMPLETE SO NOT COMPILED)
//
//   Copyright (C) 2019 Ian Jackson
//
//  plag-mangler is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.


use super:*;

impl PlanarGraph {
    pub fn consistency(&self) -> Result(<>) {
        let g = self;

        // Consistency check.  We walk the whole graph, checking
        // firstly basic properties (consistency of srcs values, lack
        // of self-edges, etc.) and also that the planar embedding is
        // correct.

        // Our planar embedding check proceeds as follows:
        // We maintain a region of the graph which we have checked.
        // In what follows a `chain' is any chain of edges.
        //
        // We mark edge edge as to whether its chain is:
        //    blue     initially
        //    yellow   part of boundary of good region
        //    red      2nd contact of face to good region
        //    green    fully inside good region
        //
        // We maintain a deque which contains at least one
        // representative edge for every chain of yellow or red edges.
        //
        // Initially, we mark all edges blue.  We pick some face and
        // mark its edges and put each of its chains on the deque.
        //
        // Our loop proceeds as follows: pick a chain from the deque.
        // If it is green pick another.
        //
        // Check to see if all red or yellow chains are contiguous.
        // If not: if any edges were red, failure; otherwise paint all
        // the edges red and put every chain of this face onto the
        // back of the deque.
        //
        // If all the red or yellow edges are contiguous, then
        // (there cannot be any green edges) mark all the red/yellow
        // edges as green and all the blue edges as yellow.  Put the
        // blue edges onto the deque *at the front*.
        //
        //  postpone this
        //
        //
        // We maintain a deque of edges to check.
        //
        // We record the boundary of that region in a deque.
        //
        // At each stage, we select an edge on this boundary.
        // 

        , and add the
        // adjacent face to our checked region.  We expect that the
